angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('auth.login', {
        url: '/login',
        templateUrl: "views/auth/login.html",
        controller: 'LoginCtrl',
        cache:false
      });
  }])
.controller('LoginCtrl', function($scope,$state,$rootScope,$ionicLoading,SecurityService) {
  $ionicLoading.hide();
  $scope.obj={
    'UserName':'',
    'Password':''
  };

//  $scope.obj={
//    'UserName':'clubibizaguard',
//    'Password':'clubibizaguard'
//  };


  $scope.doLogin = function(){
             //$state.go('dashboard');
             if($scope.obj.UserName == '' || $scope.obj.UserName==undefined){
                 alert('Please enter UserName');
                 $scope.message = 'Please enter UserName';
                 $scope.dash = true;
                 setTimeout(function ()
                        {
                          $scope.$apply(function()
                          {
                            $scope.dash = false;
                          });
                 }, 2000);
              }
             else{

               if($scope.obj.Password == '' || $scope.obj.Password==undefined){
                   $scope.message = 'Please enter Password';
                   alert('Please enter Password');
                   $scope.dash = true;
                   setTimeout(function ()
                          {
                            $scope.$apply(function()
                            {
                              $scope.dash = false;
                            });
                    }, 2000);
               }
               else{
//                 if($rootScope.locationObj.id =='' || $rootScope.locationObj.id ==undefined){
//                   $scope.message = 'Please enter Location';
//                   $scope.dash = true;
//                   setTimeout(function ()
//                          {
//                            $scope.$apply(function()
//                            {
//                              $scope.dash = false;
//                            });
//                    }, 2000);
//                 }
//                 else{
                      //$state.go('app.dashboard');
                      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
                    SecurityService.login($scope.obj).then(function(data) {
                                    //alert(JSON.stringify(data))
                                     if(data.DeploymentType != '' &&  data.DeploymentType != null && data.DeploymentType != undefined){
                                        $rootScope.useDetails = data;
                                        $state.go('app.dashboard');

                                     }
                                     else{
                                       $ionicLoading.hide();
//                                        $rootScope.useDetails = data;
//                                        $state.go('app.stockInProduct');
                                        //$state.go('app.dashboard');
                                       alert('Invalid Credintials');

                                       $scope.message = data.Message;
                                       $scope.dash = true;
                                       setTimeout(function (){
                                           $scope.$apply(function()
                                           {
                                             $scope.dash = false;
                                           });
                                       }, 2000);
                                     }

                    },function(){});
                 //}




               }
             }


  };




});
