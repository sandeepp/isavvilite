angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.visitorBlockEdit', {
        url: "/visitorBlockEdit/:id",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/visitorBlockEdit.html",
            controller: 'visitorBlockEditCtrl'

          }
        }
      });
  }])
.controller('visitorBlockEditCtrl', function($scope,$state,$rootScope,$ionicLoading,$stateParams,SecurityService) {

 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);
 // $scope.userDetails = {};
 // $scope.userDetails.DeploymentID = 29;
 // $scope.userDetails.DeploymentType = 'Condo';

 $scope.visitorId = $stateParams.id;

 $scope.visitorAddObj = {};
 $scope.visitorAddObj.deploymentid = $scope.userDetails.DeploymentID;

 $scope.setNRIC = function(){
      SecurityService.validateNRIC($scope.userDetails.DeploymentType,{ "nricid": visitorAddObj.nricid }).then(function(data) {
                                     $scope.nricInfo = data;
                                     console.log(data);

      },function(){});
 };

 $scope.getPurpose = function(){
      SecurityService.purposeList().then(function(data) {
                    $scope.purposeDetails = data;
                    if(data.length>0){
                        $scope.visitorAddObj.purpose = data[0].PurposeName;
                                     console.log(data);
                    }


      },function(){});
 };

 $scope.getPasses = function(id){
      SecurityService.passesList({ "DeploymentID": $scope.userDetails.DeploymentID,"VisitorTypeID":id }).then(function(data) {
                            $scope.passesDetails = data;
                            if(data.length>0) {
                                $scope.visitorAddObj.Passid = data[0].PassId.toString();
                                console.log(data);
                            }

      },function(){});
 };

 $scope.getVisitorTypes = function(){
      SecurityService.visitorTypesList().then(function(data) {
                          $scope.visitorTypeDetails = data;
                          if(data.length>0) {
                              $scope.visitorAddObj.visitortype = data[0].VisitorTypeId.toString();
                              console.log(data);
                              $scope.getPasses(data[0].VisitorTypeId);
                          }

      },function(){});
 };

 $scope.getKeys = function(){
      SecurityService.keysList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                            $scope.keysDetails = data;
                            if(data.length>0){
                            $scope.visitorAddObj.keyid = data[0].KeyId.toString();
                                     console.log(data);
                            }


      },function(){});
 };

 $scope.getGates = function(){
      SecurityService.gatesList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                    $scope.gatesDetails = data;
                                    if(data.length>0){
                                        $scope.visitorAddObj.gateid = data[0].GateId.toString();
                                                 console.log(data);
                                    }


      },function(){});
 };

 $scope.saveVisitorCheckIn = function(){
      $scope.visitorAddObj.checkintime = $scope.convertDate('');
      $scope.visitorAddObj.snapshotpath = '';
      $scope.visitorAddObj.status = true;
      $scope.visitorAddObj.visitorid = $scope.visitorId;

      if(!$scope.visitorAddObj.IsGuestBlocked){
          $scope.visitorAddObj.BlockType='';
          $scope.visitorAddObj.BlockedDate ='';
          $scope.visitorAddObj.BlockDays='';
          $scope.visitorAddObj.BlockRemarks='';
          $scope.visitorAddObj.UnblockedDate ='';

      }

      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
      var  visitorAddObj = angular.copy($scope.visitorAddObj);
      if($scope.visitorAddObj.BlockedDate !=='' && $scope.visitorAddObj.BlockedDate !==null){
          visitorAddObj.BlockedDate = $scope.convertDate($scope.visitorAddObj.BlockedDate);
      }
      if($scope.visitorAddObj.UnblockedDate !=='' && $scope.visitorAddObj.UnblockedDate !==null){
          visitorAddObj.UnblockedDate = $scope.convertDate($scope.visitorAddObj.UnblockedDate);
      }

      SecurityService.updateVisitorCheckIn($scope.userDetails.DeploymentType,visitorAddObj).then(function(data) {

                                     console.log(data);
                                     if(data == 'Success'){
                                         var checkouttime = $scope.convertDate('');
                                         var blockObj = {
                                              "Visitorid": visitorAddObj.visitorid,
                                              "BlockType": visitorAddObj.BlockType,
                                              "BlockDays": visitorAddObj.BlockDays,
                                              "BlockRemarks":visitorAddObj.BlockRemarks ,
                                              "Checkouttime": checkouttime,
                                              "BlockedDate": visitorAddObj.BlockedDate,
                                              "UnblockedDate": visitorAddObj.UnblockedDate,
                                              "IsGuestBlocked": true
                                         };
                                         SecurityService.checkOutVisitor($scope.userDetails.DeploymentType,blockObj).then(function(data) {
                                                $state.go('app.visitorsCheckOutList');
                                                $ionicLoading.hide();

                                         },function(){});
                                     }
                                     else{
                                         alert(data);
                                         $ionicLoading.hide();
                                     }



      },function(){});
 };

 $scope.changeVisitorStatus = function (id) {
     $scope.getPasses(id);
 };

 if($scope.userDetails.DeploymentType == 'Condo'){
         $scope.getPurpose();
         $scope.getVisitorTypes();
         $scope.getKeys();
         $scope.getGates();

 }
 $scope.changeDateCal = function(){
   var timeDiff = Math.abs($scope.visitorAddObj.UnblockedDate.getTime() - $scope.visitorAddObj.BlockedDate.getTime());
   $scope.visitorAddObj.BlockDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
 };
 $scope.getVisitorDetails = function () {
     SecurityService.getVisitorInfo($scope.userDetails.DeploymentType,{'VisitorID':$scope.visitorId}).then(function(data) {
                 $scope.visitorAddObj = data;
                 $scope.nricDisplay = $scope.hideNric($scope.visitorAddObj.nricid);
                 if($scope.userDetails.DeploymentType == 'Club' || $scope.userDetails.DeploymentType == 'Condo'){
                     if($scope.visitorAddObj.BlockType=='' || $scope.visitorAddObj.BlockType==null)
                         $scope.visitorAddObj.BlockType = 'Permanent';

                     if($scope.visitorAddObj.IsGuestBlocked){
                         if($scope.visitorAddObj.BlockedDate!=='' && $scope.visitorAddObj.BlockedDate!==null){
                             $scope.visitorAddObj.BlockedDate = new Date($scope.visitorAddObj.BlockedDate);
                         }
                         if($scope.visitorAddObj.UnblockedDate!=='' && $scope.visitorAddObj.UnblockedDate!==null){
                             $scope.visitorAddObj.UnblockedDate = new Date($scope.visitorAddObj.UnblockedDate);
                         }
                     }
                     else{
                       $scope.visitorAddObj.IsGuestBlocked = true;
                       $scope.visitorAddObj.BlockedDate = new Date();
                       $scope.visitorAddObj.UnblockedDate = new Date($scope.visitorAddObj.BlockedDate);
                       $scope.visitorAddObj.UnblockedDate.setDate($scope.visitorAddObj.BlockedDate.getDate()+1);
                       $scope.changeDateCal();
                     }



                 }
                 if($scope.visitorAddObj.hasOwnProperty('visitortype')) {
                     $scope.visitorAddObj.visitortype = $scope.visitorAddObj.visitortype.toString();
                     $scope.changeVisitorStatus($scope.visitorAddObj.visitortype);
                 }
                 if($scope.visitorAddObj.hasOwnProperty('keyid'))
                     $scope.visitorAddObj.keyid = $scope.visitorAddObj.keyid.toString();
                 if($scope.visitorAddObj.hasOwnProperty('gateid'))
                     $scope.visitorAddObj.gateid = $scope.visitorAddObj.gateid.toString();



     },function(){});
 };

 $scope.getVisitorDetails();

 $scope.convertDate = function (param) {
     var date = new Date();
     if(param!='')
        date = new Date(param);
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     if(param!='')
       format = year+'-'+month+'-'+day;
     return format;

 };

 $scope.hideNric = function(data){
    var str =data;
    var length = str.length;
    var res = "*****"+str.substring(5, length);
    return res;
 };




});
