angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.vehicalView', {
        url: "/vehicalView/:id",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/vehicalView.html",
            controller: 'vehicalViewCtrl'

          }
        }
      });
  }])
.controller('vehicalViewCtrl', function($scope,$state,$stateParams,$rootScope,$ionicLoading,SecurityService) {

 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);
 //$scope.userDetails = {};
 // $scope.userDetails.DeploymentID = 29;
 // $scope.userDetails.DeploymentType = 'Condo';

 $scope.vehicalAddObj = {};
 $scope.vehicalAddObj.deploymentid = $scope.userDetails.DeploymentID;

 $scope.setNRIC = function(){
      SecurityService.validatePlateNumber($scope.userDetails.DeploymentType,{ "platenumber": vehicalAddObj.platenumber }).then(function(data) {
                                     $scope.vehicalInfo = data;
                                     console.log(data);

      },function(){});
 };

 $scope.getPurpose = function(){
      SecurityService.purposeList().then(function(data) {
                    $scope.purposeDetails = data;
                    if(data.length>0){
                        $scope.vehicalAddObj.purpose = data[0].PurposeName;
                                     console.log(data);
                    }


      },function(){});
 };

 $scope.getvehicalTypes = function(){
      SecurityService.vehicalTypesList().then(function(data) {
                    $scope.vehicalTypeDetails = data;
                    if(data.length>0){
                        $scope.vehicalAddObj.vehicletype = data[0].vehicletype;
                                     console.log(data);
                    }


      },function(){});
 };


 $scope.savevehicalCheckIn = function(){
      $scope.vehicalAddObj.checkintime = $scope.convertDate();
      $scope.vehicalAddObj.imagepath = '';
      if($scope.vehicalAddObj.platenumber == '' || $scope.vehicalAddObj.platenumber == undefined || $scope.vehicalAddObj.platenumber == null){
          alert('Vehical Plate # is Mandatory');
          return '';
      }
      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });

      SecurityService.updateVehicalCheckIn($scope.userDetails.DeploymentType,$scope.vehicalAddObj).then(function(data) {

                                     console.log(data);
                                     $state.go('app.vehicalsList');
                                     $ionicLoading.hide();

      },function(){});
 };

 $scope.getVehicalDetails = function () {
     SecurityService.getVehicalInfo($scope.userDetails.DeploymentType,{'vehicleid':$stateParams.id}).then(function(data) {
            $scope.vehicalAddObj = data;
      },function(){});
 };


 if($scope.userDetails.DeploymentType == 'Condo'){
         $scope.getPurpose();
         $scope.getvehicalTypes();
         $scope.getVehicalDetails();

 }

 $scope.convertDate = function () {
     var date = new Date();
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     return format;

 };




});
