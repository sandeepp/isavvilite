angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.visitorAdd', {
        url: "/visitorAdd",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/visitorAdd.html",
            controller: 'visitorAddCtrl'

          }
        }
      });
  }])
.controller('visitorAddCtrl', function($scope,$state,$rootScope,$ionicLoading,$ionicPopup,$cordovaCamera,SecurityService) {

 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);
 // $scope.userDetails = {};
 // $scope.userDetails.DeploymentID = 27;
 // $scope.userDetails.DeploymentType = 'Club';
 $scope.imgURI = '';
 $scope.nricObj = {};
 $scope.nricObj.nricDisplay = '';
 $scope.nricObj.nricDisplayOriginal = '';
 $scope.visitorAddObj = {};
 $scope.visitorAddObj.nricid = '';
 $scope.visitorAddObj.telno = '';
 $scope.visitorAddObj.snapshotpath = '';
 $scope.visitorAddObj.deploymentid = $scope.userDetails.DeploymentID;
 $scope.visitorAddObj.NRICFront = '';
 $scope.visitorAddObj.NRICBack = '';
 $scope.documentType = 'ic';

 $scope.setNRIC = function(){
      SecurityService.validateNRIC($scope.userDetails.DeploymentType,{ "nricid": $scope.visitorAddObj.nricid }).then(function(data) {
                                     $scope.nricInfo = data;
                                     console.log(data);

      },function(){});
 };

 $scope.getPurpose = function(){
      SecurityService.purposeList().then(function(data) {
                    $scope.purposeDetails = data;
                    if(data.length>0){
                        $scope.visitorAddObj.purpose = data[0].PurposeName;
                                     console.log(data);
                    }


      },function(){});
 };

 $scope.getPasses = function(id){
      SecurityService.passesList({ "DeploymentID": $scope.userDetails.DeploymentID,"VisitorTypeID":id }).then(function(data) {
                            $scope.passesDetails = data;
                            if(data.length>0) {
                                $scope.visitorAddObj.Passid = data[0].PassId.toString();
                                console.log(data);
                            }

      },function(){});
 };

 $scope.getVisitorTypes = function(){
      SecurityService.visitorTypesList().then(function(data) {
                          $scope.visitorTypeDetails = data;
                          if(data.length>0) {
                              $scope.visitorAddObj.visitortype = data[0].VisitorTypeId.toString();
                              console.log(data);
                              $scope.getPasses(data[0].VisitorTypeId);
                          }

      },function(){});
 };

 $scope.getKeys = function(){
      SecurityService.keysList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                            $scope.keysDetails = data;
                            if(data.length>0){
                            $scope.visitorAddObj.keyid = data[0].KeyId.toString();
                                     console.log(data);
                            }


      },function(){});
 };

 $scope.getGates = function(){
      SecurityService.gatesList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                    $scope.gatesDetails = data;
                                    if(data.length>0){
                                        $scope.visitorAddObj.gateid = data[0].GateId.toString();
                                                 console.log(data);
                                    }


      },function(){});
 };

 $scope.saveVisitorCheckIn = function(){
      $scope.visitorAddObj.checkintime = $scope.convertDate();
      $scope.visitorAddObj.status = true;
      if($scope.userDetails.DeploymentType=='Club'){
          if($scope.nricObj.nricDisplay == '' || $scope.nricObj.nricDisplay == undefined || $scope.nricObj.nricDisplay == null){
              alert('NRIC is Mandatory');
              return '';
          }
          else if($scope.visitorAddObj.name == '' || $scope.visitorAddObj.name == undefined || $scope.visitorAddObj.name == null){
              alert('Name is Mandatory');
              return '';
          }

          else if($scope.visitorAddObj.BlockType == 'Temporary'){
              if($scope.visitorAddObj.BlockDays=='' || $scope.visitorAddObj.BlockDays==null || $scope.visitorAddObj.BlockDays==undefined){
                 alert('Block Days is Mandatory');
                 return '';
              }
          }
      }
      else if($scope.userDetails.DeploymentType=='Condo'){
          if($scope.visitorAddObj.telno == '' || $scope.visitorAddObj.telno == undefined || $scope.visitorAddObj.telno == null){
              alert('Tel # is Mandatory');
              return '';
          }
          else if($scope.visitorAddObj.name == '' || $scope.visitorAddObj.name == undefined || $scope.visitorAddObj.name == null){
              alert('Name is Mandatory');
              return '';
          }

          else if($scope.visitorAddObj.BlockType == 'Temporary'){
              if($scope.visitorAddObj.BlockDays=='' || $scope.visitorAddObj.BlockDays==null || $scope.visitorAddObj.BlockDays==undefined){
                 alert('Block Days is Mandatory');
                 return '';
              }
          }
      }

      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });


      var obj = angular.copy($scope.visitorAddObj);
      obj.snapshotpath = $scope.imgURI;

      //alert(obj.snapshotpath);
      //$scope.chaeckBlockStatus($scope.visitorAddObj.nricid);
      if($scope.userDetails.DeploymentType == 'Club' || $scope.userDetails.DeploymentType=='Condo'){
         $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });

             var obj1={};
             obj1.deploymentid = $scope.userDetails.DeploymentID;
             if($scope.visitorAddObj.nricid!=''){
                obj1.nricid = $scope.visitorAddObj.nricid;
             }
             if($scope.visitorAddObj.telno!='' && $scope.visitorAddObj.telno!=undefined){
                obj1.telno = $scope.visitorAddObj.telno;
             }
         SecurityService.chaeckVisitorBlockStatus($scope.userDetails.DeploymentType,obj1).then(function(data) {
                $ionicLoading.hide();
                if(data.IsGuestBlocked){
                        if(data.BlockType == 'Temporary'){
                            var GivenDate = new Date(data.UnblockedDate);
                            var CurrentDate = new Date();


                            if(CurrentDate>GivenDate){
                               SecurityService.visitorCheckIn($scope.userDetails.DeploymentType,obj).then(function(data) {

                                         console.log(data);
                                          $ionicLoading.hide();
                                         if(data=='Success') {
                                             $state.go('app.visitorsList');
                                         }
                                         else{
                                             alert(data);
                                         }


                               },function(){});
                            }else{
                                var myPopup1 = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate+' <b>Unblocked Date :</b>'+data.UnblockedDate+' <b>Blocked Days :</b>'+data.BlockDays,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                                });

                            }
                        }
                        else{
                            var myPopup = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                            });

                        }

                }else{
                    SecurityService.visitorCheckIn($scope.userDetails.DeploymentType,obj).then(function(data) {

                                         console.log(data);
                                          $ionicLoading.hide();
                                         if(data=='Success') {
                                             $state.go('app.visitorsList');
                                         }
                                         else{
                                             alert(data);
                                         }


                    },function(){});
                }

         },function(err){
            $ionicLoading.hide();
             alert(JSON.stringify(err));
         });
      }else{


          SecurityService.visitorCheckIn($scope.userDetails.DeploymentType,obj).then(function(data) {

                                         console.log(data);
                                          $ionicLoading.hide();
                                         if(data=='Success') {
                                             $state.go('app.visitorsList');
                                         }
                                         else{
                                             alert(data);
                                         }


          },function(){});
      }
 };

 $scope.changeVisitorStatus = function (id) {
     $scope.getPasses(id);
 };

 if($scope.userDetails.DeploymentType == 'Condo'){
         $scope.getPurpose();
         $scope.getVisitorTypes();
         $scope.getKeys();
         $scope.getGates();

 }

 $scope.convertDate = function () {
     var date = new Date();
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     return format;

 };

 $scope.cheackValue = function(){
    $scope.nricObj.nricDisplay = angular.copy($scope.nricObj.nricDisplayOriginal);
 };

 $scope.chaeckBlockStatus = function (type) {
     if(type == 'nric'){
         $scope.nricObj.nricDisplayOriginal = angular.copy($scope.nricObj.nricDisplay);
         $scope.visitorAddObj.nricid = angular.copy($scope.nricObj.nricDisplay);
         if($scope.nricObj.nricDisplay!=''){
            $scope.nricObj.nricDisplay = angular.copy($scope.hideNric($scope.nricObj.nricDisplay));
         }
     }

     if($scope.userDetails.DeploymentType == 'Club' || $scope.userDetails.DeploymentType == 'Condo'){

         if($scope.visitorAddObj.nricid!='' || $scope.visitorAddObj.telno!='') {
             $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
             var obj={};
             obj.deploymentid = $scope.userDetails.DeploymentID;
             if($scope.visitorAddObj.nricid!=''){
                obj.nricid = $scope.visitorAddObj.nricid;
             }
             if($scope.visitorAddObj.telno!='' && $scope.visitorAddObj.telno!=undefined){
                obj.telno = $scope.visitorAddObj.telno;
             }
             SecurityService.chaeckVisitorBlockStatus($scope.userDetails.DeploymentType,obj).then(function(data) {
                    $ionicLoading.hide();
                    if(data.IsGuestBlocked){
                        if(data.BlockType == 'Temporary'){
                            var GivenDate = new Date(data.UnblockedDate);
                            var CurrentDate = new Date();


                            if(CurrentDate>GivenDate){
                                return;
                            }else{
                                var myPopup = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate+' <b>Unblocked Date :</b>'+data.UnblockedDate+' <b>Blocked Days :</b>'+data.BlockDays,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                                });
                            }
                        }
                        else{
                            var myPopup2 = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                            });
                        }

                    }

             },function(err){
                 $ionicLoading.hide();
                 alert(JSON.stringify(err));
             });
         }
     }

 };

 $scope.uploadFromCamera = function () {
    //alert('Upload from Camera');

    var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
    };
    //alert('open');

    $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.visitorAddObj.snapshotpath  =  "data:image/jpeg;base64," +imageData;
        $scope.imgURI = imageData;
        //alert($scope.imgURI);


    }, function (err) {
      alert('error'+err);

    });
 };
    var types = ["SingaporeIDFront","SingaporeIDBack"];
	var imageTypes = ["IMAGE_SUCCESSFUL_SCAN", "IMAGE_FACE", "IMAGE_DOCUMENT"];
	var language = "en";

	// Note that each platform requires its own license key

	// This license key allows setting overlay views for this application ID: com.microblink.blinkid
	var licenseiOs = "HPZUQOIV-V6OE6YMP-KCP56FUY-7SYEA3ND-2TXQVZFF-5SFW6RTF-X2QO3IHN-UDW5P6OM"; // valid until 2018-05-09

	// This license is only valid for package name "com.microblink.blinkid"
	// https://samliew.com/nric-generator
	var licenseAndroid = "HPZUQOIV-V6OE6YMP-KCP56FUY-7SYEA3ND-2TXQVZFF-5SFW6RTF-X2QO3IHN-UDW5P6OM";
	var resultDiv = document.getElementById('innerDiv');
	resultDiv.innerHTML = 'hi';
	  $scope.scan = function () {

		cordova.plugins.blinkIdScanner.scan(

			// Register the callback handler
			function callback(scanningResult) {
                //alert("data"+JSON.stringify(scanningResult));
				// handle cancelled scanning

//				if (scanningResult.cancelled == true) {
//					resultDiv.innerHTML = "Cancelled!";
//					return;
//				}
//
//				// Obtain list of recognizer results
				var resultList = scanningResult.resultList;
                //alert("result"+JSON.stringify(scanningResult));
//
//				successfulImageDiv.style.visibility = "hidden";
//				documentImageDiv.style.visibility = "hidden";
//				faceImageDiv.style.visibility = "hidden";
//
//				// Image is returned as Base64 encoded JPEG
//				var image = scanningResult.resultImage;
//
//				// Successful image is returned as Base64 encoded JPEG
//				var resultSuccessfulImage = scanningResult.resultSuccessfulImage;
//				if (resultSuccessfulImage) {
//					successfulImage.src = "data:image/jpg;base64, " + resultSuccessfulImage;
//					successfulImageDiv.style.visibility = "visible";
//				}

				// Iterate through all results
				for (var i = 0; i < resultList.length; i++) {

					// Get individual resilt
					var recognizerResult = resultList[i];
					var fields = recognizerResult.fields;
					var resultImage = recognizerResult.resultDocumentImage;
					if(resultImage){
					   if(types[0] == 'SingaporeIDFront'){
					     //alert("front");
					     $scope.visitorAddObj.NRICFront = resultImage;
					   }
					   else{
					     //alert("back");
					     $scope.visitorAddObj.NRICBack = resultImage;
					   }
					}

					if (recognizerResult.resultType == "Barcode result") {
						// handle Barcode scanning result

						var raw = "";
						if (typeof(recognizerResult.raw) != "undefined" && recognizerResult.raw != null) {
							raw = " (raw: " + hex2a(recognizerResult.raw) + ")";
						}
						resultDiv.innerHTML = "Data: " + recognizerResult.data +
											raw +
											" (Type: " + recognizerResult.type + ")";

					} else if (recognizerResult.resultType == "USDL result") {
						// handle USDL parsing result

						resultDiv.innerHTML = /** Personal information */
											"USDL version: " + fields[kPPStandardVersionNumber] + "<br>" +
											"Family name: " + fields[kPPCustomerFamilyName] + "<br>" +
											"First name: " + fields[kPPCustomerFirstName] + "<br>" +
											"Date of birth: " + fields[kPPDateOfBirth] + "<br>" +
											"Sex: " + fields[kPPSex] + "<br>" +
											"Eye color: " + fields[kPPEyeColor] + "<br>" +
											"Height: " + fields[kPPHeight] + "<br>" +
											"Street: " + fields[kPPAddressStreet] + "<br>" +
											"City: " + fields[kPPAddressCity] + "<br>" +
											"Jurisdiction: " + fields[kPPAddressJurisdictionCode] + "<br>" +
											"Postal code: " + fields[kPPAddressPostalCode] + "<br>" +

											/** License information */
											"Issue date: " + fields[kPPDocumentIssueDate] + "<br>" +
											"Expiration date: " + fields[kPPDocumentExpirationDate] + "<br>" +
											"Issuer ID: " + fields[kPPIssuerIdentificationNumber] + "<br>" +
											"Jurisdiction version: " + fields[kPPJurisdictionVersionNumber] + "<br>" +
											"Vehicle class: " + fields[kPPJurisdictionVehicleClass] + "<br>" +
											"Restrictions: " + fields[kPPJurisdictionRestrictionCodes] + "<br>" +
											"Endorsments: " + fields[kPPJurisdictionEndorsementCodes] + "<br>" +
											"Customer ID: " + fields[kPPCustomerIdNumber] + "<br>";

					} else if (recognizerResult.resultType == "MRTD result" || recognizerResult.resultType == "UnitedArabEmiratesIDBack result") {
						// UnitedArabEmiratesIDBack result contains only fields from the MRZ (Machine Readable Zone)
                         //alert("mrtd"+JSON.stringify(fields));
                         $scope.visitorAddObj.nricid = fields.Opt1.substring(0, 9);
                         $scope.nricObj.nricDisplay = angular.copy($scope.visitorAddObj.nricid);
                         $scope.nricObj.nricDisplay = angular.copy($scope.hideNric($scope.nricObj.nricDisplay));
                         if(fields.SecondaryId==undefined || fields.SecondaryId == "" || fields.SecondaryId == null){
                            $scope.visitorAddObj.name = fields.PrimaryId;
                         }
                         else{
                            $scope.visitorAddObj.name = fields.SecondaryId;

                         }
//						resultDiv.innerHTML = /** Personal information */
//											"Type: " + fields[kPPDataType] + "<br>" +
//											"Family name: " + fields[kPPmrtdPrimaryId] + "<br>" +
//											"First name: " + fields[kPPmrtdSecondaryId] + "<br>" +
//											"Date of birth: " + fields[kPPmrtdBirthDate] + "<br>" +
//											"Sex: " + fields[kPPmrtdSex] + "<br>" +
//											"Nationality: " + fields[kPPmrtdNationality] + "<br>" +
//											"Date of Expiry: " + fields[kPPmrtdExpiry] + "<br>" +
//											"Document Code: " + fields[kPPmrtdDocCode] + "<br>" +
//											"Document Number: " + fields[kPPmrtdDocNumber] + "<br>" +
//											"Issuer: " + fields[kPPmrtdIssuer] + "<br>" +
//											"ID Type: " + fields[kPPmrtdDataType] + "<br>" +
//											"Opt1: " + fields[kPPmrtdOpt1] + "<br>" +
//											"Opt2: " + fields[kPPmrtdOpt2] + "<br>";

					} else if (recognizerResult.resultType == "EUDL result" || recognizerResult.resultType == "UKDL result" || recognizerResult.resultType == "DEDL result") {

						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"Date of Expiry: " + fields[kPPeudlExpiry] + "<br>" +
											"Issue Date: " + fields[kPPeudlIssueDate] + "<br>" +
											"Issuing Authority: " + fields[kPPeudlIssuingAuthority] + "<br>" +
											"Driver Number: " + fields[kPPeudlDriverNumber] + "<br>" +
											"Address: " + fields[kPPeudlAddress] + "<br>" +
											"Birth Data: " + fields[kPPeudlBirthData] + "<br>" +
											"First name: " + fields[kPPeudlFirstName] + "<br>" +
											"Last name: " + fields[kPPeudlLastName] + "<br>";

					} else if (recognizerResult.resultType == "MyKad result") {

						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"NRIC Number: " + fields[kPPmyKadNricNumber] + "<br>" +
											"Address: " + fields[kPPmyKadAddress] + "<br>" +
											"Address ZIP Code: " + fields[kPPmyKadAddressZipCode] + "<br>" +
											"Address Street: " + fields[kPPmyKadAddressStreet] + "<br>" +
											"Address City: " + fields[kPPmyKadAddressCity] + "<br>" +
											"Address State: " + fields[kPPmyKadAddressState] + "<br>" +
											"Birth Date: " + fields[kPPmyKadBirthDate] + "<br>" +
											"Full Name: " + fields[kPPmyKadFullName] + "<br>" +
											"Religion: " + fields[kPPmyKadReligion] + "<br>" +
											"Sex: " + fields[kPPmyKadSex] + "<br>";

					} else if (recognizerResult.resultType == "GermanOldID result") {


						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"Family name: " + fields[kPPmrtdPrimaryId] + "<br>" +
											"First name: " + fields[kPPmrtdSecondaryId] + "<br>" +
											"Date of birth: " + fields[kPPmrtdBirthDate] + "<br>" +
											"Nationality: " + fields[kPPmrtdNationality] + "<br>" +
											"Document Code: " + fields[kPPmrtdDocCode] + "<br>" +
											"Document Number: " + fields[kPPmrtdDocNumber] + "<br>" +
											"Issuer: " + fields[kPPmrtdIssuer] + "<br>" +
											"Place of birth: " + fields[kPPgermanIdBirthPlace] + "<br>";

					} else if (recognizerResult.resultType == "GermanFrontID result") {

						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"Last name: " + fields[kPPgermanIdLastName] + "<br>" +
											"First name: " + fields[kPPgermanIdFirstName] + "<br>" +
											"Date of birth: " + fields[kPPgermanIdBirthDate] + "<br>" +
											"Place of birth: " + fields[kPPgermanIdBirthPlace] + "<br>" +
											"Nationality: " + fields[kPPgermanIdNationality] + "<br>" +
											"Date of expiry: " + fields[kPPgermanIdExpiryDate] + "<br>" +
											"Card number: " + fields[kPPgermanIdCardNumber] + "<br>";

					} else if (recognizerResult.resultType == "GermanBackID result") {

						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"Colour of eyes: " + fields[kPPgermanIdEyeColour] + "<br>" +
											"Height: " + fields[kPPgermanIdHeight] + "<br>" +
											"Issue date: " + fields[kPPgermanIdIssueDate] + "<br>" +
											"Issuing authority: " + fields[kPPgermanIdIssuingAuthority] + "<br>" +
											"Address: " + fields[kPPgermanIdAddress] + "<br>";

					} else if (recognizerResult.resultType == "GermanPassport result") {

						resultDiv.innerHTML = /** Personal information */
											"Type: " + fields[kPPDataType] + "<br>" +
											"Passport number: " + fields[kPPmrtdDocNumber] + "<br>" +
											"Surname: " + fields[kPPgermanPassSurname] + "<br>" +
											"Name: " + fields[kPPgermanPassName] + "<br>" +
											"Nationality: " + fields[kPPgermanPassNationality] + "<br>" +
											"Date of birth: " + fields[kPPmrtdBirthDate] + "<br>" +
											"Sex: " + fields[kPPmrtdSex] + "<br>" +
											"Place of birth: " + fields[kPPgermanPassBirthPlace] + "<br>" +
											"Date of issue: " + fields[kPPgermanPassIssueDate] + "<br>" +
											"Date of expiry: " + fields[kPPmrtdExpiry] + "<br>" +
											"Authority: " + fields[kPPgermanPassIssuingAuthority] + "<br>";

					} else if (recognizerResult.resultType == "UnitedArabEmiratesIDFront result") {

						resultDiv.innerHTML = /** Personal information */
											"ID Type: " + fields[kPPDataType] + "<br>" +
											"ID number: " + fields[kPPuaeIdFrontIdNumber] + "<br>" +
											"Name: " + fields[kPPuaeIdFrontName] + "<br>" +
											"Nationality: " + fields[kPPuaeIdFrontNationality] + "<br>";

					} else if (recognizerResult.resultType == "SingaporeFrontID result") {

					    $scope.visitorAddObj.nricid = fields['SingaporeIDCardNumberFront.CardNumber'];
					    $scope.nricObj.nricDisplay = angular.copy($scope.visitorAddObj.nricid);
                        $scope.nricObj.nricDisplay = angular.copy($scope.hideNric($scope.nricObj.nricDisplay));
					    $scope.visitorAddObj.name = fields['SingaporeIDName.Name'];

//						resultDiv.innerHTML = /** Personal information */
//											"ID Type: " + fields[kPPDataType] + "<br>" +
//											"Card number: " + fields[kPPsingaporeCardNumberFront] + "<br>" +
//											"Date of birth: " + fields[kPPsingaporeDateOfBirth] + "<br>" +
//											"Country of birth: " + fields[kPPsingaporeCountryOfBirth] + "<br>" +
//											"Race: " + fields[kPPsingaporeRace] + "<br>" +
//											"Name: " + fields[kPPsingaporeName] + "<br>" +
//											"Sex: " + fields[kPPsingaporeSex] + "<br>";

					} else if (recognizerResult.resultType == "SingaporeBackID result") {

                         $scope.visitorAddObj.address = fields['SingaporeIDAddress.Address'];
//						resultDiv.innerHTML = /** Personal information */
//											"ID Type: " + fields[kPPDataType] + "<br>" +
//											"Card number: " + fields[kPPsingaporeCardNumberBack] + "<br>" +
//											"Date of issue: " + fields[kPPsingaporeDateOfIssue] + "<br>" +
//											"Blood group: " + fields[kPPsingaporeBloodGroup] + "<br>" +
//											"Address: " + fields[kPPsingaporeAddress] + "<br>";

					} else if (recognizerResult.resultType == "DocumentDetector result") {

						resultDiv.innerHTML = "Found a document";

					} else if (recognizerResult.resultType == "DocumentFace result") {

						resultDiv.innerHTML = "Found document with face";

					} else {

						resultDiv.innerHTML = recognizerResult.resultType;

					}

					// Document image is returned as Base64 encoded JPEG
//					var resultDocumentImage = recognizerResult.resultDocumentImage;
//					if (resultDocumentImage) {
//						documentImage.src = "data:image/jpg;base64, " + resultDocumentImage;
//						documentImageDiv.style.visibility = "visible";
//					} else {
//						documentImageDiv.style.visibility = "hidden";
//					}
//
//					// Face image is returned as Base64 encoded JPEG
//					var resultFaceImage = recognizerResult.resultFaceImage;
//					if (resultFaceImage) {
//						faceImage.src = "data:image/jpg;base64, " + resultFaceImage;
//						faceImageDiv.style.visibility = "visible";
//					} else {
//						faceImageDiv.style.visibility = "hidden";
//					}
				}
			},

			// Register the error callback
			function errorHandler(err) {
				alert('Error: ' + err);
			},

			types, imageTypes, licenseiOs, licenseAndroid, language
		);
	  };

	  $scope.scanAll = function () {
		types = ["SingaporeIDFront","SingaporeIDBack"];
		$scope.scan();
	  };
	  $scope.scanFront = function () {
		types = ["SingaporeIDFront"];
		$scope.scan();
	  };
	  $scope.scanBack = function () {
		types = ["SingaporeIDBack"];
		$scope.scan();
	  };

	  $scope.scanFrontPassport = function(){
	    types = ["MRTD"];
		$scope.scan();
	  };

      $scope.hideNric = function(data){
            var str =data;
            var length = str.length;
            var res = "*****"+str.substring(5, length);
            return res;
      };






});
