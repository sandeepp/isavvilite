angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.visitorsCheckOutList', {
        url: "/visitorsCheckOutList",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/visitorCheckOutList.html",
            controller: 'visitorCheckOutCtrl'

          }
        }
      });
  }])
.controller('visitorCheckOutCtrl', function($scope,$state,$rootScope,$ionicLoading,$ionicPopup,SecurityService) {
 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);

 $scope.getVisitorsList = function(){
      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
      SecurityService.visitorsList($scope.userDetails.DeploymentType,{ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                     $scope.visitorsList = [];
                                     if(data.length>0){

                                        $scope.visitorsList = data;
                                        $ionicLoading.hide();

                                     }
                                     else{
                                       $ionicLoading.hide();
                                       $scope.message = data.Message;
                                       $scope.dash = true;
                                       setTimeout(function (){
                                           $scope.$apply(function()
                                           {
                                             $scope.dash = false;
                                           });
                                       }, 2000);
                                     }

      },function(){});
 };

 $scope.getVisitorsList();

 $scope.currentDate = new Date();

 $scope.viewVisitor = function (obj) {
     $state.go('app.visitorView',{id:obj.visitorid});

 };

 $scope.checkOutVisitorInList = function (obj) {


         var myPopup = $ionicPopup.show({
            template: 'Are you sure you want to CheckOut  ?',
            title: 'Confirmation',
            scope: $scope,
            buttons: [
                {
                    text: 'No',
                    type: 'button',
                    onTap: function(e) {

                    }
                },
                {
                    text: 'Yes',
                    type: 'button-positive',
                    onTap: function(e) {
                         $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                         });
                         var checkouttime = $scope.convertDate();
                         SecurityService.checkOutVisitor($scope.userDetails.DeploymentType,{'VisitorID':obj.visitorid,'checkouttime':checkouttime}).then(function(data) {

                                                     console.log(data);
                                                     $scope.getVisitorsList();


                         },function(){});
                    }
                }
            ]
        });
 };

 $scope.getDate = function (d) {
     var date = new Date(d);
    return date.toString();
 };
    
 $scope.convertDate = function () {
     var date = new Date();
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     return format;

 };
  
 $scope.blockUser = function (visitor) {
     var myPopup = $ionicPopup.show({
            template: 'Are you sure you want to Block the Visitor ?',
            title: 'Confirmation',
            scope: $scope,
            buttons: [
                {
                    text: 'No',
                    type: 'button',
                    onTap: function(e) {

                    }
                },
                {
                    text: 'Yes',
                    type: 'button-positive',
                    onTap: function(e) {
                          $state.go('app.visitorBlockEdit',{id:visitor.visitorid});

                    }
                }
            ]
     });

 };

  $scope.hideNric = function(data){
    var str =data;
    var length = str.length;
    var res = "*****"+str.substring(5, length);
    return res;
 };





});
