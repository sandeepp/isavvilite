angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.vehicalCheckout', {
        url: "/vehicalCheckoutList",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/vehicalCheckout.html",
            controller: 'vehicalCheckoutListCtrl'

          }
        }
      });
  }])
.controller('vehicalCheckoutListCtrl', function($scope,$state,$rootScope,$ionicLoading,$ionicPopup,SecurityService) {
 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);

 $scope.getVehicalsList = function(){
      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
      SecurityService.vehicalsList($scope.userDetails.DeploymentType,{ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                     $scope.vehicalList = [];
                                     if(data.length>0){

                                        $scope.vehicalList = data.reverse();
                                        $ionicLoading.hide();

                                     }
                                     else{
                                       $ionicLoading.hide();
                                       $scope.message = data.Message;
                                       $scope.dash = true;
                                       setTimeout(function (){
                                           $scope.$apply(function()
                                           {
                                             $scope.dash = false;
                                           });
                                       }, 2000);
                                     }

      },function(){});
 };

 $scope.getVehicalsList();

 $scope.currentDate = new Date();

 $scope.updateVehical = function (obj) {
     $state.go('app.vehicalView',{id:obj.vehicleid});

 };

 $scope.checkoutViehicalInList = function (obj) {


         var myPopup = $ionicPopup.show({
            template: 'Are you sure you want to Checkout the Vehical in List.',
            title: 'Confirmation',
            scope: $scope,
            buttons: [
                {
                    text: 'No',
                    type: 'button',
                    onTap: function(e) {

                    }
                },
                {
                    text: 'Yes',
                    type: 'button-positive',
                    onTap: function(e) {
                         $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                         });
                         var checkouttime = $scope.convertDate();
                         SecurityService.checkOutVehical($scope.userDetails.DeploymentType,{'vehicleid':obj.vehicleid,'checkouttime':checkouttime}).then(function(data) {


                                                     console.log(data);
                                                     $scope.getVehicalsList();


                         },function(){});
                    }
                }
            ]
        });
 };

 $scope.getDate = function (d) {
     var date = new Date(d);
    return date.toString();
 };

 $scope.convertDate = function () {
     var date = new Date();
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     return format;

 };





});
