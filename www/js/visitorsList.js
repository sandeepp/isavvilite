angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.visitorsList', {
        url: "/visitorsList",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/visitorsList.html",
            controller: 'visitorsListCtrl'

          }
        }
      });
  }])
.controller('visitorsListCtrl', function($scope,$state,$rootScope,$ionicLoading,$ionicPopup,SecurityService) {
 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);

 $scope.getVisitorsList = function(){
      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
      SecurityService.visitorsList($scope.userDetails.DeploymentType,{ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                     $scope.visitorsList = [];
                                     if(data.length>0){

                                        $scope.visitorsList = data;
                                        $ionicLoading.hide();

                                     }
                                     else{
                                       $ionicLoading.hide();
                                       $scope.message = data.Message;
                                       $scope.dash = true;
                                       setTimeout(function (){
                                           $scope.$apply(function()
                                           {
                                             $scope.dash = false;
                                           });
                                       }, 2000);
                                     }

      },function(){});
 };

 $scope.getVisitorsList();

 $scope.currentDate = new Date();

 $scope.updateVisitor = function (obj) {
     $state.go('app.visitorEdit',{id:obj.visitorid});

 };

 $scope.deleteVisitorInList = function (obj) {


         var myPopup = $ionicPopup.show({
            template: 'Are you sure you want to Delete the Visitor.',
            title: 'Confirmation',
            scope: $scope,
            buttons: [
                {
                    text: 'No',
                    type: 'button',
                    onTap: function(e) {

                    }
                },
                {
                    text: 'Yes',
                    type: 'button-positive',
                    onTap: function(e) {
                         $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                         });
                         SecurityService.deleteVisitor($scope.userDetails.DeploymentType,{'VisitorID':obj.visitorid}).then(function(data) {

                                                     console.log(data);
                                                     $scope.getVisitorsList();


                         },function(){});
                    }
                }
            ]
        });
 };

 $scope.getDate = function (d) {
     var date = new Date(d);
    return date.toString();
 };

 $scope.hideNric = function(data){
    var str =data;
    var length = str.length;
    var res = "*****"+str.substring(5, length);
    return res;
 };





});
