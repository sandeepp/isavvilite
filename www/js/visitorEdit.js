angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.visitorEdit', {
        url: "/visitorEdit/:id",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/visitorEdit.html",
            controller: 'visitorEditCtrl'

          }
        }
      });
  }])
.controller('visitorEditCtrl', function($scope,$state,$rootScope,$ionicLoading,$ionicPopup,$stateParams,$cordovaCamera,SecurityService) {

 $ionicLoading.hide();

 $scope.userDetails = angular.copy($rootScope.useDetails);
 // $scope.userDetails = {};
 // $scope.userDetails.DeploymentID = 29;
 // $scope.userDetails.DeploymentType = 'Condo';

 $scope.imgURI = '';
 $scope.is_edit = false;
 $scope.visitorId = $stateParams.id;

 $scope.visitorAddObj = {};
 $scope.visitorAddObj.deploymentid = $scope.userDetails.DeploymentID;
 $scope.visitorAddObj.snapshotpath = '';
 $scope.nricObj = {};

 $scope.setNRIC = function(){
      SecurityService.validateNRIC($scope.userDetails.DeploymentType,{ "nricid": visitorAddObj.nricid }).then(function(data) {
                                     $scope.nricInfo = data;
                                     console.log(data);

      },function(){});
 };

 $scope.getPurpose = function(){
      SecurityService.purposeList().then(function(data) {
                    $scope.purposeDetails = data;
                    if(data.length>0){
                        $scope.visitorAddObj.purpose = data[0].PurposeName;
                                     console.log(data);
                    }


      },function(){});
 };

 $scope.getPasses = function(id){
      SecurityService.passesList({ "DeploymentID": $scope.userDetails.DeploymentID,"VisitorTypeID":id }).then(function(data) {
                            $scope.passesDetails = data;
                            if(data.length>0) {
                                $scope.visitorAddObj.Passid = data[0].PassId.toString();
                                console.log(data);
                            }

      },function(){});
 };

 $scope.getVisitorTypes = function(){
      SecurityService.visitorTypesList().then(function(data) {
                          $scope.visitorTypeDetails = data;
                          if(data.length>0) {
                              $scope.visitorAddObj.visitortype = data[0].VisitorTypeId.toString();
                              console.log(data);
                              $scope.getPasses(data[0].VisitorTypeId);
                          }

      },function(){});
 };

 $scope.getKeys = function(){
      SecurityService.keysList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                            $scope.keysDetails = data;
                            if(data.length>0){
                            $scope.visitorAddObj.keyid = data[0].KeyId.toString();
                                     console.log(data);
                            }


      },function(){});
 };

 $scope.getGates = function(){
      SecurityService.gatesList({ "DeploymentID": $scope.userDetails.DeploymentID }).then(function(data) {
                                    $scope.gatesDetails = data;
                                    if(data.length>0){
                                        $scope.visitorAddObj.gateid = data[0].GateId.toString();
                                                 console.log(data);
                                    }


      },function(){});
 };

 $scope.saveVisitorCheckIn = function(){
      $scope.visitorAddObj.checkintime = $scope.convertDate('');
      $scope.visitorAddObj.snapshotpath = '';
      $scope.visitorAddObj.status = true;
      $scope.visitorAddObj.visitorid = $scope.visitorId;
      if($scope.userDetails.DeploymentType=='Club'){
          if($scope.nricObj.nricDisplay == '' || $scope.nricObj.nricDisplay == undefined || $scope.nricObj.nricDisplay == null){
              alert('NRIC is Mandatory');
              return '';
          }
          else if($scope.visitorAddObj.name == '' || $scope.visitorAddObj.name == undefined || $scope.visitorAddObj.name == null){
              alert('Name is Mandatory');
              return '';
          }

          else if($scope.visitorAddObj.BlockType == 'Temporary'){
              if($scope.visitorAddObj.BlockDays=='' || $scope.visitorAddObj.BlockDays==null || $scope.visitorAddObj.BlockDays==undefined){
                 alert('Block Days is Mandatory');
                 return '';
              }
          }
      }
      else if($scope.userDetails.DeploymentType=='Condo'){
          if($scope.visitorAddObj.telno == '' || $scope.visitorAddObj.telno == undefined || $scope.visitorAddObj.telno == null){
              alert('Tel # is Mandatory');
              return '';
          }
          else if($scope.visitorAddObj.name == '' || $scope.visitorAddObj.name == undefined || $scope.visitorAddObj.name == null){
              alert('Name is Mandatory');
              return '';
          }

          else if($scope.visitorAddObj.BlockType == 'Temporary'){
              if($scope.visitorAddObj.BlockDays=='' || $scope.visitorAddObj.BlockDays==null || $scope.visitorAddObj.BlockDays==undefined){
                 alert('Block Days is Mandatory');
                 return '';
              }
          }
      }

      if(!$scope.visitorAddObj.IsGuestBlocked){
          $scope.visitorAddObj.BlockType='';
          $scope.visitorAddObj.BlockedDate ='';
          $scope.visitorAddObj.BlockDays='';
          $scope.visitorAddObj.BlockRemarks='';
          $scope.visitorAddObj.UnblockedDate ='';

      }

      $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
      var  visitorAddObj = angular.copy($scope.visitorAddObj);
      visitorAddObj.snapshotpath = $scope.imgURI;
      if($scope.visitorAddObj.BlockedDate !=='' && $scope.visitorAddObj.BlockedDate !==null){
          visitorAddObj.BlockedDate = $scope.convertDate($scope.visitorAddObj.BlockedDate);
      }
      if($scope.visitorAddObj.UnblockedDate !=='' && $scope.visitorAddObj.UnblockedDate !==null){
          visitorAddObj.UnblockedDate = $scope.convertDate($scope.visitorAddObj.UnblockedDate);
      }

      $scope.checkBlockState =  $scope.chaeckBlockStatus('edit');
      if(!$scope.checkBlockState){
      SecurityService.updateVisitorCheckIn($scope.userDetails.DeploymentType,visitorAddObj).then(function(data) {
                                     if($scope.visitorAddObj.IsGuestBlocked){
                                          $scope.checkOutVisitor(visitorAddObj);
                                          $ionicLoading.hide();
                                     }
                                     else{
                                         $state.go('app.visitorsList');
                                     }
                                     console.log(data);

                                     $ionicLoading.hide();

      },function(){});
      }
 };

 $scope.changeVisitorStatus = function (id) {
     $scope.getPasses(id);
 };

 $scope.checkOutVisitor = function(obj){
     $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
     var checkouttime = $scope.convertDate('');
     var blockObj = {
          "Visitorid": obj.visitorid,
          "BlockType": obj.BlockType,
          "BlockDays": obj.BlockDays,
          "BlockRemarks":obj.BlockRemarks ,
          "Checkouttime": checkouttime,
          "BlockedDate": obj.BlockedDate,
          "UnblockedDate": obj.UnblockedDate,
          "IsGuestBlocked": true
     };
     SecurityService.checkOutVisitor($scope.userDetails.DeploymentType,blockObj).then(function(data) {

                                 console.log(data);
                                 $state.go('app.visitorsList');
                                 $ionicLoading.hide();


     },function(){});
 };

 if($scope.userDetails.DeploymentType == 'Condo'){
         $scope.getPurpose();
         $scope.getVisitorTypes();
         $scope.getKeys();
         $scope.getGates();

 }

 $scope.changeDateCal = function(){
   var timeDiff = Math.abs($scope.visitorAddObj.UnblockedDate.getTime() - $scope.visitorAddObj.BlockedDate.getTime());
   $scope.visitorAddObj.BlockDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
 };

 $scope.getVisitorDetails = function () {
     $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
     SecurityService.getVisitorInfo($scope.userDetails.DeploymentType,{'VisitorID':$scope.visitorId}).then(function(data) {
                 $ionicLoading.hide();
                 $scope.visitorAddObj = data;
                 $scope.nricObj.nricDisplayOriginal = angular.copy($scope.visitorAddObj.nricid);
                 $scope.nricObj.nricDisplay = $scope.hideNric($scope.visitorAddObj.nricid);
                 if($scope.userDetails.DeploymentType == 'Club' || $scope.userDetails.DeploymentType == 'Condo' ){
                     if($scope.visitorAddObj.BlockType=='' || $scope.visitorAddObj.BlockType==null)
                         $scope.visitorAddObj.BlockType = 'Permanent';

                     if($scope.visitorAddObj.IsGuestBlocked=='' || $scope.visitorAddObj.IsGuestBlocked==null) {
                         $scope.visitorAddObj.IsGuestBlocked = false;
                         $scope.visitorAddObj.BlockedDate = new Date();
                         $scope.visitorAddObj.UnblockedDate = new Date($scope.visitorAddObj.BlockedDate);
                         $scope.visitorAddObj.UnblockedDate.setDate($scope.visitorAddObj.BlockedDate.getDate()+1);
                         $scope.changeDateCal();

                     }

                     if($scope.visitorAddObj.IsGuestBlocked){
                         if($scope.visitorAddObj.BlockedDate!=='' && $scope.visitorAddObj.BlockedDate!==null){
                             $scope.visitorAddObj.BlockedDate = new Date($scope.visitorAddObj.BlockedDate);
                         }
                         if($scope.visitorAddObj.UnblockedDate!=='' && $scope.visitorAddObj.UnblockedDate!==null){
                             $scope.visitorAddObj.UnblockedDate = new Date($scope.visitorAddObj.UnblockedDate);
                         }
                     }



                 }
                 if($scope.visitorAddObj.hasOwnProperty('visitortype')) {
                     $scope.visitorAddObj.visitortype = $scope.visitorAddObj.visitortype.toString();
                     $scope.changeVisitorStatus($scope.visitorAddObj.visitortype);
                 }
                 if($scope.visitorAddObj.hasOwnProperty('keyid'))
                     $scope.visitorAddObj.keyid = $scope.visitorAddObj.keyid.toString();
                 if($scope.visitorAddObj.hasOwnProperty('gateid'))
                     $scope.visitorAddObj.gateid = $scope.visitorAddObj.gateid.toString();



     },function(){
         $ionicLoading.hide();
     });
 };

 $scope.getVisitorDetails();

 $scope.convertDate = function (param) {
     var date = new Date();
     if(param!='')
        date = new Date(param);
     var year = date.getFullYear();
     var month = date.getMonth() + 1;
     var day = date.getDate();
     var hour = date.getHours();
     var minute = date.getMinutes();
     var sec = date.getSeconds();
     var format = year+'-'+month+'-'+day+" "+hour+":"+minute+":"+sec;
     if(param!='')
       format = year+'-'+month+'-'+day;
     return format;

 };

 $scope.cheackValue = function(){
    $scope.nricObj.nricDisplay = angular.copy($scope.nricObj.nricDisplayOriginal);
 };

 $scope.chaeckBlockStatus = function (type) {
     if(type == 'new'){
         $scope.visitorAddObj.nricid = angular.copy($scope.nricObj.nricDisplay);
         if($scope.nricObj.nricDisplay!=''){
            $scope.nricObj.nricDisplay = angular.copy($scope.hideNric($scope.nricObj.nricDisplay));
         }
     }
     if($scope.userDetails.DeploymentType == 'Club' || $scope.userDetails.DeploymentType == 'Condo'){
         $ionicLoading.show({
                              template: '<ion-spinner icon="bubbles"></ion-spinner>'
                      });
         if($scope.visitorAddObj.nricid!='' || $scope.visitorAddObj.telno!='') {
             var obj={};
             obj.deploymentid = $scope.userDetails.DeploymentID;
             if($scope.visitorAddObj.nricid!=''){
                obj.nricid = $scope.visitorAddObj.nricid;
             }
             else{
                if($scope.visitorAddObj.telno!='' && $scope.visitorAddObj.telno!=undefined){
                    obj.telno = $scope.visitorAddObj.telno;
                }
             }

             SecurityService.chaeckVisitorBlockStatus($scope.userDetails.DeploymentType,obj).then(function(data) {
                    $ionicLoading.hide();
                    if(data.IsGuestBlocked){
                        if(data.BlockType == 'Temporary'){
                            var GivenDate = new Date(data.UnblockedDate);
                            var CurrentDate = new Date();


                            if(CurrentDate>GivenDate){
                                return false;
                            }else{
                                var myPopup1 = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate+' <b>Unblocked Date :</b>'+data.UnblockedDate+' <b>Blocked Days :</b>'+data.BlockDays,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                                });
                                return true;
                            }
                        }
                        else{
                            var myPopup = $ionicPopup.show({
                                    template: '<b>Block Type: </b>'+data.BlockType+'<b> Blocked On :</b>'+data.BlockedDate,
                                    title: 'Confirmation',
                                    scope: $scope,
                                    buttons: [
                                        {
                                            text: 'OK',
                                            type: 'button-positive',
                                            onTap: function(e) {
                                                  $state.go('app.visitorsList');

                                            }
                                        }
                                    ]
                            });
                            return true;
                        }

                    }
                    else{
                       return false;
                    }

             },function(){});
         }
     }

 };

  $scope.uploadFromCamera = function () {
    //alert('Upload from Camera');

    var options = {
        quality: 75,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 300,
        targetHeight: 300,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false
    };
    //alert('open');

    $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.visitorAddObj.snapshotpath  =  "data:image/jpeg;base64," +imageData;
         $scope.imgURI = imageData;
         $scope.is_edit = true;
        //alert($scope.imgURI);


    }, function (err) {
      alert('error'+err);

    });
  };

   $scope.hideNric = function(data){
    var str =data;
    var length = str.length;
    var res = "*****"+str.substring(5, length);
    return res;
  };




});
