angular.module('your_app_name')

.factory('SecurityService', function($rootScope, $q, $http) {
  $rootScope.visitor_path ='http://158.140.133.89/iSavviLite/snapshots/visitors/';
  $rootScope.vehical_path ='http://158.140.133.89/iSavviLite/snapshots/vehicles/';

    var serverUrl = (ionic.Platform.is('browser')) ? '' : 'http://158.140.133.89';
    //var serverUrl = '';
    var _login=function(obj){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/general/AuthenticateAppUser";
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _visitorsList=function(type,obj){
            var endPoint = 'GetCondoVisitorsCheckInsList';
            if(type == 'Club')
                endPoint = 'GetClubVisitorsCheckInsList';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _validateNRIC=function(type,obj){
            var endPoint = 'FillCondoVisitorDetails';
            if(type == 'Club')
                endPoint = 'FillClubVisitorDetails';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _purposeList=function(){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/GetPurposesList";
            var deferred = $q.defer();
                 $http.post(path,{}).success(function(data) {
                    deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _passesList=function(obj){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/GetPasses";
            var deferred = $q.defer();
                 $http.post(path,obj).success(function(data) {
                    deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };


    var _visitorTypesList=function(){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/GetVisitorTypes";
            var deferred = $q.defer();
                 $http.post(path,{}).success(function(data) {
                    deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };


    var _keysList=function(obj){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/GetKeys";
            var deferred = $q.defer();
                 $http.post(path,obj).success(function(data) {
                    deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };


    var _gatesList=function(obj){
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/GetGates";
            var deferred = $q.defer();
                 $http.post(path,obj).success(function(data) {
                    deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _visitorCheckIn=function(type,obj){
            var endPoint = 'AddCondoVisitorCheckIn';
            if(type == 'Club')
                endPoint = 'AddClubVisitorCheckIn';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _getVisitorInfo=function(type,obj){
            var endPoint = 'GetCondoVisitorsCheckInDetails';
            if(type == 'Club')
                endPoint = 'GetClubVisitorCheckInDetails';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _updateVisitorCheckIn=function(type,obj){
            var endPoint = 'UpdateCondoVisitorCheckInDetails';
            if(type == 'Club')
                endPoint = 'UpdateClubVisitorCheckInDetails';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(obj));
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _deleteVisitor=function(type,obj){
            var endPoint = 'DeleteCondoVisitorCheckInDetails';
            if(type == 'Club')
                endPoint = 'DeleteClubVisitorCheckInDetails';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _checkOutVisitor=function(type,obj){
            var endPoint = 'SetCondoVisitorCheckOutStatus';
            if(type == 'Club')
                endPoint = 'SetClubVisitorCheckOutStatus';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _visitorsBlockList=function(type,obj){
            var endPoint = '';
            if(type == 'Club')
                endPoint = 'GetBlockedClubVisitorsList';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _unblockVisitor=function(type,obj){
            var endPoint = '';
            if(type == 'Club')
                endPoint = 'UnblockClubVisitor';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _chaeckVisitorBlockStatus=function(type,obj){
            var endPoint = 'checkcondovisitorblockstatus';
            if(type == 'Club')
                endPoint = 'CheckClubVisitorBlockStatus';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/visitor/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _vehicalsList=function(type,obj){
            var endPoint = 'getvehiclecheckinlist';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };

    var _deleteVehical=function(type,obj){
            var endPoint = 'deletevehiclecheckindetails';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _vehicalTypesList=function(type,obj){
            var endPoint = 'getvehicletypes';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,{}).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _vehicalCheckIn=function(type,obj){
            var endPoint = 'addvehiclecheckindetails';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _validatePlateNumber=function(type,obj){
            var endPoint = 'fillvehicledetails';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _getVehicalInfo=function(type,obj){
            var endPoint = 'getvehiclecheckindetails';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _updateVehicalCheckIn=function(type,obj){
            var endPoint = 'updatevehiclecheckindetails';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };
    var _checkOutVehical=function(type,obj){
            var endPoint = 'setvehiclecheckoutstatus';
            if(type == 'Club')
                endPoint = '';
            var path =serverUrl+"/iSavviLite/iSavviLiteAPI/api/Vehicle/"+endPoint;
            var deferred = $q.defer();
                 //alert("true"+path+"data"+JSON.stringify(data))
                 $http.post(path,obj).success(function(data) {
                    //alert("ser"+JSON.stringify(data))

                     deferred.resolve(data);
                 }).error(function(data) {
                     deferred.reject({
                         error: data
                     });
                 });

           return deferred.promise;
    };







 var securityService = {

     login:_login,
     visitorsList:_visitorsList,
     validateNRIC:_validateNRIC,
     purposeList:_purposeList,
     passesList:_passesList,
     visitorTypesList:_visitorTypesList,
     keysList:_keysList,
     gatesList:_gatesList,
     visitorCheckIn:_visitorCheckIn,
     getVisitorInfo:_getVisitorInfo,
     updateVisitorCheckIn:_updateVisitorCheckIn,
     deleteVisitor:_deleteVisitor,
     checkOutVisitor:_checkOutVisitor,
     visitorsBlockList:_visitorsBlockList,
     chaeckVisitorBlockStatus:_chaeckVisitorBlockStatus,
     unblockVisitor:_unblockVisitor,
     vehicalsList:_vehicalsList,
     deleteVehical:_deleteVehical,
     vehicalTypesList:_vehicalTypesList,
     vehicalCheckIn:_vehicalCheckIn,
     validatePlateNumber:_validatePlateNumber,
     getVehicalInfo:_getVehicalInfo,
     updateVehicalCheckIn:_updateVehicalCheckIn,
     checkOutVehical:_checkOutVehical



 };

 return securityService;

});
