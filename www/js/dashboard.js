angular.module('your_app_name')
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('app.dashboard', {
        url: "/dashboard",
        cache:false,
        views: {
          'menuContent': {
            templateUrl: "views/app/feeds/dashboard.html",
            controller: 'dashboardCtrl'

          }
        }
      });
  }])
.controller('dashboardCtrl', function($scope,$state,$rootScope,$ionicLoading,SecurityService) {

 $ionicLoading.hide();
 $scope.userDetails = angular.copy($rootScope.useDetails);



});
